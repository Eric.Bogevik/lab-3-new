package no.uib.inf101.terminal;
public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        String test = "";
        for (String string : args) {
            test = test + string + " ";
        }
        return test;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "echo";
    }
    
}
